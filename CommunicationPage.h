﻿#ifndef H_MY__COMMUNICATIONPAGE_H__H
#define	H_MY__COMMUNICATIONPAGE_H__H
#include <QWidget>
#include "ui_CommunicationPage.h"
#include "ATabPageBase.h"
class CommShowMsgItem;

namespace widgetUI {
	class CommunicationPage;
}

class widgetUI::CommunicationPage : public ATabPageBase {
Q_OBJECT;
public:
	void activation(QObject* obj, int index, EVENTFLAG status) override;
	QString getName( ) override;
	QIcon getIcon( ) override;
private:
public:
	CommunicationPage(QWidget* parent = Q_NULLPTR);
	/*static CommunicationPage* getInstance( );*/
	void addMsgItem(CommShowMsgItem* msgItem);
	void testMsgItem(int itemCount, int msgCountLen);
	~CommunicationPage( );
	QScrollBar* getVScrollBar( );
	QScrollBar* getHScrollBar( );
	QScrollArea* getScrollArea( );
	QWidget* getMsgBox( );
private:
	Ui::CommunicationPage ui;
signals:
	void appIndex(CommShowMsgItem* msgItem);
};
#endif // H_MY__COMMUNICATIONPAGE_H__H
