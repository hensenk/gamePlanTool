﻿#ifndef H_MY__ITABPAGESLOTSEVENT_H__H
#define	H_MY__ITABPAGESLOTSEVENT_H__H
#include <qwidget.h>

namespace widgetUI {
	class ATabPageBase;
}

class widgetUI::ATabPageBase : public QWidget {
Q_OBJECT;
public:
	ATabPageBase(QWidget* parent): QWidget(parent) { }

	enum EVENTFLAG {
		DoubleClicked
		, Clicked
		, Changed
	};

	// 当前页面被激活
	virtual void activation(QObject* obj, int index, EVENTFLAG status) = 0;
	// 获取名称
	virtual QString getName( ) = 0;
	// 获取图标
	virtual QIcon getIcon( ) = 0;
};
#endif // H_MY__ITABPAGESLOTSEVENT_H__H
