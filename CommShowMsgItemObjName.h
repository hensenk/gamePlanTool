﻿#ifndef H_MY__ICOMMSHOWMSGITEM_H__H
#define	H_MY__ICOMMSHOWMSGITEM_H__H
#include <QWidget>

namespace itemName {
	class CommShowMsgItemObjName;
}

class itemName::CommShowMsgItemObjName {
public:
	static QString getUserNameObjName( );
	static QString getUserImageObjName( );
	static QString getMsgLabelObjName( );
	static QString getItemObjName( );
	QString getMsgBoxObjName( );
};
#endif // H_MY__ICOMMSHOWMSGITEM_H__H
