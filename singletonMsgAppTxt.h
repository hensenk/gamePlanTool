﻿#ifndef H_MY__SINGLETONMSGAPPTXT_H__H
#define	H_MY__SINGLETONMSGAPPTXT_H__H
#include "IMsgStream.h"

namespace appInstances {
	class MsgAppTxt;
}

class appInstances::MsgAppTxt {
public:
	static msg::IMsgAppTxt* instance(msg::IMsgAppTxt* instance = Q_NULLPTR);
};
#endif // H_MY__SINGLETONMSGAPPTXT_H__H
