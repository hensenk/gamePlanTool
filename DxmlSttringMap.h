﻿#ifndef H_MY__DXMLSTTRINGMAP_H__H
#define	H_MY__DXMLSTTRINGMAP_H__H
#include "dXml.h"
#include "SappInstance.h"
#include "XmlDataDoc.h"

class appInstances::DxmlSttringMap {
	DxmlSttringMap( );
	QHash<QString, dXml::XmlDataDoc*>* map;
	dXml::XmlDataDoc* defaultDoc;
public:
	static DxmlSttringMap* getInstance( );

	class xmlPrit {
	public:
		QString fileName;
		dXml::XmlDataDoc* xmlFileObj;

		xmlPrit(QString fileName
			, dXml::XmlDataDoc* xmlFileObj) : fileName{ fileName },
			xmlFileObj{ xmlFileObj } {}
	};

	~DxmlSttringMap( );
	xmlPrit getXmlFile(QString& fileName);
	const dXml::XmlDataDoc* getDefaultDocXmlFile( );
	xmlPrit addXmlFile(QString& fileName);
};
#endif // H_MY__DXMLSTTRINGMAP_H__H
