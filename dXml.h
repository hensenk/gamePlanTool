﻿#ifndef H_MY__DXML_H__H
#define	H_MY__DXML_H__H
#include <QDomDocument>

namespace dXml {
	// 解析数学表达式
	namespace MathExpress {
		/*
		 * 数学表达式管理类
		 * 用于管理解析与模型的生产
		 */
		class MathManage;
		/*
		 * 表达式解析类，用于生产模型类
		 */
		class MathExpressResolve;
		/*
		 * 数学表达式数据结构
		 * 它是一个只读模型，需要时候请使用 Manage/MathExpressResolve 对他进行管理
		 */
		class MathExpressBody;
		/*
		 * 配对子
		 * 用于 MathExpressBody 与 MathExpressResolve 的关联
		 */
		class MathPairUnit;
	}

	class XmlDataDoc;
	class XmlDataUnit;
	// node 转换到 QDomElement 树
	QDomElement toDocElement(QDomDocument& doc
		, XmlDataUnit* node);
	// 从 QDomElement 转 node
	void domDocumentToDxmlUnitDoc(dXml::XmlDataUnit* saveToData
		, QString& outPITarget
		, QString& outPIData
		, QDomDocument doc);
	// 存储到 file
	// nodes 多个根节点
	bool save(QList<XmlDataUnit*>& nodes
		, const QString& file
		, QDomDocument* qDomDocument = Q_NULLPTR
		, int tabSize = 4
		, QDomNode::EncodingPolicy edcoding = QDomNode::EncodingFromDocument);
	// 数学公式转 xml
	dXml::XmlDataDoc toMaths(const QString& expression);
	dXml::XmlDataDoc toMaths(const char* szExpression);
	/*
	 * saveToDataList 用于存储结果
	 * outPITarget 存储 ProcessingInstructionTarget
	 * outPIData 存储 ProcessingInstructionData
	 */
	bool readXmlFile(XmlDataUnit* saveToData
		, QString& outPITarget
		, QString& outPIData
		, const QString& file);
	bool readXmlFile(XmlDataUnit* saveToData
		, const QString& file);
	/*
	 * 从文本中读取到xml
	 */
	bool readStringToXml(XmlDataUnit* saveToData
		, QString& outPITarget
		, QString& outPIData
		, const QString& strStream);
	/*
	 * QDomNodeList 转 QList<dXml::XmlDataUnit*> 
	 */
	QList<dXml::XmlDataUnit*> docChildRootNodesToXmlDataUnity(const QDomNodeList& domNodes
		, XmlDataUnit* paren);
	/*
	 * 节点混合
	 */
	XmlDataDoc xmlBlend(const dXml::XmlDataDoc& leftDoc
		, const dXml::XmlDataDoc& rightDoc
		, int dupStatus = 0);
	/*
	 * 混合功能排序函数
	 * 大到小
	 */
	int xmlBlendUnitSortOrder(const XmlDataUnit* left
		, const XmlDataUnit* right);
	/*
	 * 混合功能排序函数
	 * 小到大
	 */
	int xmlBlendUnitSortReOrder(const XmlDataUnit* left
		, const XmlDataUnit* right);
	QVector<XmlDataUnit*> xmlBlendUnitList(QList<XmlDataUnit*>* leftDoc
		, QList<XmlDataUnit*>* rightDoc
		, int dupStatus = 0);

	inline const char* getProcessingInstructionTarget( ) {
		static const char* instance = "xml";
		return instance;
	}

	inline const char* getProcessingInstructionData( ) {
		static const char* instance = "version=\"1.0\" encoding=\"UTF-8\"";
		return instance;
	}

	inline const char* getDefaultXmlxStr( ) {
		static const char* instance = "<?xml version=\"1.0\" encoding=\"utf-8\"?><setting><language select=\"zh-CN\"><zh-CN>zh-cn.lg</zh-CN><en>en.lg</en></language></setting>";
		return instance;
	}
}
#endif // H_MY__DXML_H__H
