﻿#include "singletonPageWidgetS.h"

::PageWidgetS* appInstances::PageWidgetS::GetSigInstanceInstance( ) {
	QMutexLocker locker(::PageWidgetS::m_mutex( ));
	static ::PageWidgetS* instance = Q_NULLPTR;
	if( instance == Q_NULLPTR )
		instance = new ::PageWidgetS;
	return instance;
}