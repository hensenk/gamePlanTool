﻿#ifndef H_MY__PAGEWIDGETS_H__H
#define	H_MY__PAGEWIDGETS_H__H
#include <QMutexLocker>
#include <QWidget>

namespace appInstances{
	class PageWidgetS;
}
class PageWidgetS {
public:
	friend class appInstances::PageWidgetS;
	virtual ~PageWidgetS( );
	QHash<QString, QWidget*>& GetNameHashMap( );
protected:
	static QMutex* m_mutex( );
private:
	PageWidgetS( );
};
#endif // H_MY__PAGEWIDGETS_H__H
