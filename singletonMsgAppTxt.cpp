﻿#include "singletonMsgAppTxt.h"
#include "SappInstance.h"

msg::IMsgAppTxt* appInstances::MsgAppTxt::instance(msg::IMsgAppTxt* instance) {
	QMutexLocker locker(MsgLocker::getMutex( ));
	static msg::IMsgAppTxt* obj = Q_NULLPTR;
	if( instance == Q_NULLPTR ) {
		if( obj ) {
			bool valid = obj->objIsValid( );
			if( valid != true )
				obj = Q_NULLPTR;
		}
		return obj;
	}
	bool valid = instance->objIsValid( );
	if( valid == true )
		obj = instance;
	return obj;
}
