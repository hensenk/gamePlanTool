﻿#include "SappInstance.h"
#include "SigInstance.h"
#include "PageWidgetS.h"

QMutex* appInstances::MsgLocker::getMutex( ) {
	static QMutex instance;
	return &instance;
}
