﻿#pragma once
#include <QWidget>
#include "ui_MsgPage.h"
#include "IMsgStream.h"
#include "ATabPageBase.h"

namespace widgetUI {
	class MsgPage;
}

class widgetUI::MsgPage : public ATabPageBase, public msg::IMsgStream, public msg::IMsgAppTxt {
Q_OBJECT;
public:
	bool objIsValid( ) override;
	IMsgStream* getMsgStreamInstance( ) override;
	IMsgStream* getMsgAppTxtInstance( ) override;
	void activation(QObject* obj, int index, EVENTFLAG status) override;
public:
	MsgPage(QWidget* parent = Q_NULLPTR);
	~MsgPage( );
	IMsgStream& operator<<(const QString& msgTest) override;
	IMsgStream& operator<<(const QList<QString>& msgTest) override;
	IMsgStream& operator<<(const QStringList& msgTest) override;
public:
	bool objValidStatus(bool* status = Q_NULLPTR);
	void appendMsg(const QString& msg, const QString& appChars) override;
	void appendMsg(const QStringList& msg, const QString& appChars) override;
	void appendMsg(const QList<QString>& msg, const QString& appChars) override;
private:
	Ui::MsgPage ui;
protected:
	static QString JoinChars(QString chars = "");
public:
	QString getName( ) override;
	QIcon getIcon( ) override;
/*public:
	static MsgPage* getInstance( );*/
};
