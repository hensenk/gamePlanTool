﻿#include "singletonMsgStream.h"
#include "SappInstance.h"

msg::IMsgStream* appInstances::MsgStream::instance(msg::IMsgStream* instance) {
	QMutexLocker locker(appInstances::MsgLocker::getMutex( ));
	static msg::IMsgStream* obj = Q_NULLPTR;
	if( instance == Q_NULLPTR ) {
		if( obj ) {
			bool valid = obj->objIsValid( );
			if( valid != true )
				obj = Q_NULLPTR;
		}
		return obj;
	}
	bool valid = instance->objIsValid( );
	if( valid == true )
		obj = instance;
	return obj;
}
