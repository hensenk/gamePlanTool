﻿#ifndef H_MY__IMSGSTREAM_H__H
#define	H_MY__IMSGSTREAM_H__H
#include <QStringList>
#include <qlist.h>
#include <QStringList>

namespace msg {
	class IMsgStream;
	class IMsgAppTxt;
	class IValid;
}

class msg::IValid {
public:
	virtual bool objIsValid( ) = 0;
};

class msg::IMsgStream : public IValid {
public:
	virtual IMsgStream& operator<<(const QString& msgTest) = 0;
	virtual IMsgStream& operator<<(const QList<QString>& msgTest) = 0;
	virtual IMsgStream& operator<<(const QStringList& msgTest) = 0;
	virtual IMsgStream* getMsgStreamInstance() = 0;
	
};

class msg::IMsgAppTxt : public IValid {
public:
	virtual void appendMsg(const QString& msg, const QString& appChars) = 0;
	virtual void appendMsg(const QStringList& msg, const QString& appChars) = 0;
	virtual void appendMsg(const QList<QString>& msg, const QString& appChars) = 0;
	virtual IMsgStream* getMsgAppTxtInstance() = 0;
};
#endif // H_MY__IMSGSTREAM_H__H
