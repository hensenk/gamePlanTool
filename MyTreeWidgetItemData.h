﻿#ifndef H_MY__MYTREEWIDGETITEMDATA_H__H
#define	H_MY__MYTREEWIDGETITEMDATA_H__H
#include <QTreeWidgetItem>

class MyTreeWidgetItemData : public QTreeWidgetItem {
public:
	MyTreeWidgetItemData(QTreeWidget* treeview, const QStringList& strings, QString fullPath, int type = Type);
	MyTreeWidgetItemData(QTreeWidgetItem* parent, const QStringList& strings, QString fullPath, int type = Type);
	MyTreeWidgetItemData(const QStringList& strings, QString fullPath, int type = Type);
	QString fullPath;
	~MyTreeWidgetItemData( );
};
#endif // H_MY__MYTREEWIDGETITEMDATA_H__H
