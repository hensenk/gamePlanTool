﻿#ifndef H_MY__LANGUAGE_H__H
#define	H_MY__LANGUAGE_H__H
#include <qboxlayout.h>
#include "AToolBoxPage.h"
#include <QRadioButton>
#include <QGroupBox>
#include "dXml.h"

namespace widgetUI {
	class LanguageToolPage;
}

class widgetUI::LanguageToolPage : public widgetUI::AToolBoxPage {
public:
	void sortWidget(bool order = false) override;
	void clear( ) override;
private:
Q_OBJECT;
public:
private:
	QString objPageName = "语言设置";
	QIcon objPageIcon;
	QVBoxLayout* mainLayout;
	QGroupBox* controlGroupBox;
	QVBoxLayout* controlGroupLayout;
	dXml::XmlDataUnit* xmlLanguageUnit = Q_NULLPTR;
	QHash<QRadioButton*, QString>* map = Q_NULLPTR;
private:
	void appendItems(QList<QWidget*>& result, dXml::XmlDataUnit*& mode);
public:
	LanguageToolPage(QWidget* paren = Q_NULLPTR);
	~LanguageToolPage( );
	void activation(QObject* obj, int index) override;
	void setTitle(QString& str) override;
	dXml::XmlDataUnit getXmlUnityToCloneObj( ) override;
	QString getName( ) override;
	QIcon getIcon( ) override;
	void setName(QString& name) override;
	void setIcon(QIcon& icon) override;
public:
	QList<QWidget*> setXmlUnityDom(dXml::XmlDataUnit* mode);
	QList<QWidget*> setXmlUnityDoms(QList<dXml::XmlDataUnit*>& modes);
	QWidget* addItem(QIcon icon, const QString& msg, const QString& Data, QHash<QString, QString>* attrMap, bool isChicked);
	bool synchXmlUnit( ) override;
	QWidget* addItem(const QString& msg, const QString& Data, QHash<QString, QString>* attrMap, bool isChicked);
	QWidget* addItem(const QString& msg, const QString& Data, QHash<QString, QString>* attrMap = Q_NULLPTR);
private:
	// 链接信号
	void ConnectRadioButton(QRadioButton* signalsObj);
public: // 信号处理
	void toggledItem(bool checked);
};
#endif // H_MY__LANGUAGE_H__H
