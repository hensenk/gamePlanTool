﻿#include "MenuPage.h"
#include <QPushButton>
#include <QSpacerItem>
#include <QLineEdit>
#include "LanguageToolPage.h"
#include "XmlDataUnit.h"
#include "XmlDataDoc.h"
#include <QDir>
#include "FileBuffSystem.h"
#include "SappInstance.h"
using namespace widgetUI;

void MenuPage::addListItem(const QString& fileFullPath, const QString& fileName) {
	listBox->addItem(fileName, fileFullPath);
}

void MenuPage::activation(QObject* obj, int index, EVENTFLAG status) {
	qDebug( ) << getName( );
}

QString MenuPage::getName( ) {
	static QString objName = "菜单";
	return objName;
}

QIcon MenuPage::getIcon( ) {
	static QIcon icon = QApplication::style( )->standardIcon(( QStyle::StandardPixmap )30);
	return icon;
}

void MenuPage::initLayout(QBoxLayout* layout) {
	layout->setContentsMargins(0, 0, 0, 0);
	layout->setSpacing(0);
}

void MenuPage::initUi( ) {
	// 添加主要布局
	mainLayout = new QVBoxLayout;
	initLayout(mainLayout);
	setLayout(mainLayout);
	// 创建 Combo box
	listBox = new QComboBox( );
	fileName = new QLineEdit(this);
	readBtn = new QPushButton("读取文件");
	writeBtn = new QPushButton("保存到文件");
	allSaveKeppBtn = new QPushButton("信息保存更改");
	head = new QHBoxLayout;
	listBoxHead = new QHBoxLayout;
	initLayout(head);
	listBoxHead->addWidget(listBox, 2, Qt::AlignLeft);
	QSpacerItem* spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
	head->addSpacerItem(spacer);
	head->addWidget(fileName);
	head->addWidget(writeBtn);
	
	head->addWidget(readBtn);
	head->addWidget(allSaveKeppBtn);
	mainLayout->addLayout(listBoxHead, 1);
	mainLayout->addLayout(head, 1);
	// 添加滚动区域
	areaBox = new QScrollArea(this);
	mainLayout->addWidget(areaBox, 8);
	areaBox->setWidgetResizable(true);
	areaBox->setStyleSheet(QString::fromUtf8("QScrollBar{width:100px;}"));
	// 添加选项布局项
	areaBoxWidget = new QWidget;
	areaBox->setWidget(areaBoxWidget);
	tabPageMainLayout = new QVBoxLayout(areaBoxWidget);
	initLayout(tabPageMainLayout);
	// 添加工具卡控件
	mainToolbox = new QToolBox(areaBoxWidget);
	mainToolbox->setStyleSheet(QString::fromUtf8("QScrollBar{width:100px;}"));
	tabPageMainLayout->addWidget(mainToolbox);
	translatingToolPage = new TranslatingToolPage(this);
	mainToolbox->addItem(translatingToolPage, translatingToolPage->getIcon( ), translatingToolPage->getName( ));
	// 添加进度条
	progressBar = new QProgressBar( );
	progressBar->setAlignment(Qt::AlignHCenter);
	progressBar->setRange(0, 100);
	progressBar->setValue(100);
	progressBar->setFormat("%p%");
	// 添加组件
	mainLayout->addWidget(progressBar, 1);
}

MenuPage::MenuPage(QWidget* parent) : ATabPageBase(parent) {
	pages = new QHash<AToolBoxPage*, QByteArray*>;
	initUi( );
	initConnect( );
	// 添加默认的映射
	clearListBox( );
}

MenuPage::~MenuPage( ) {
	delete translatingToolPage;
}

void MenuPage::initConnect( ) {
	void ( QComboBox::* asignal)(int) = &QComboBox::highlighted;
	void ( MenuPage::* slot)(int) = &MenuPage::highlighted;
	connect(listBox, asignal, this, slot);
	connect(readBtn, &QPushButton::clicked, this, &MenuPage::readListItem);
	connect(writeBtn, &QPushButton::clicked, this, &MenuPage::saveFile);
	connect(allSaveKeppBtn, &QPushButton::clicked, this, &MenuPage::allSave);
}

void MenuPage::initDefaultSttingXml( ) {
	defautDoc = appInstances::FileBuffSystem::fileInstance.getDocValue(appInstances::FileBuffSystem::fileInstance.defaultXmlStting);
}

void MenuPage::xmlToTranslatingStting(dXml::XmlDataDoc* doc) {
	QList<dXml::XmlDataUnit*> xmlDataUnits = doc->findChildrens(translatingToolPage->getName( ));
	translatingToolPage->setXmlUnityDoms(xmlDataUnits);
	translatingToolPage->sortWidget( );
}

void MenuPage::initDefaultLanguageStting( ) {
	xmlToTranslatingStting(&defautDoc);
	dXml::XmlDataDoc doc;
	translatingToolPage->synchXmlUnit( );
	dXml::XmlDataUnit dataUnit = translatingToolPage->getXmlUnityToCloneObj( );
	dXml::XmlDataUnit* children = new dXml::XmlDataUnit(dataUnit);
	doc.appendChildren(children);
	doc.save(QDir::currentPath( ) + "/xml/test.xml");
}

void MenuPage::clearListBox( ) {
	listBox->clear( );
	listBox->addItem("default.xml.setting", ":/xml/stting.xml");
}

void MenuPage::saveFile(bool chickel) {
	QString dir = appInstances::FileBuffSystem::fileInstance.getCurrentDir( );
	dXml::XmlDataDoc doc;
	QString fileNaneQStr = fileName->text( );
	if( fileNaneQStr.isEmpty( ) ) {
		fileNaneQStr = "default";
		fileName->setText(fileNaneQStr);
	}
	dXml::XmlDataUnit* translUnit = new dXml::XmlDataUnit{ translatingToolPage->getXmlUnityToCloneObj( ) };
	doc.appendChildren(translUnit);
	if( dir.isEmpty( ) )
		dir = QDir::currentPath( );
	if( ! fileNaneQStr.endsWith(".xml") )
		fileNaneQStr.append(".xml");
	QString fileFullName = dir + "/" + fileNaneQStr;
	QFile file(fileFullName);
	if( file.exists( ) ) {
		QDir dirObj;
		QString targetDir = dir + "/bak/";
		dirObj.mkpath(targetDir);
		const QString TargetFileName = targetDir + "/" + QDateTime::currentDateTime( ).toString("yyyy.MM.dd.hh.mm.ss.zzz") + '.' + fileNaneQStr + ".bak";
		QFile::rename(fileFullName, TargetFileName);
	}
	doc.save(fileFullName);
	appInstances::FileBuffSystem::fileInstance.insertMapUnity(fileFullName, doc);
	fileName->setText(fileNaneQStr);
	int count = listBox->count( );
	for( int index = 0;index < count;++index ) {
		QString fileName = listBox->itemText(index);
		if( fileNaneQStr == fileName )
			return;
	}
	listBox->addItem(fileNaneQStr, fileFullName);
}

void MenuPage::allSave(bool chickel) {
	dXml::XmlDataDoc xmlDataDoc = translatingToolPage->getItemsToDoc( );
	translatingToolPage->clear( );
	xmlToTranslatingStting(&xmlDataDoc);
	isUpdate = true;
	emit saveFile(chickel);
}

void MenuPage::readListItem(bool chickel) {
	QVariant itemData = listBox->currentData( );
	QString fileFullPath = itemData.toString( );
	QFile file(fileFullPath);
	if( !file.exists( ) )
		return;
	dXml::XmlDataDoc doc;
	QString outPiTarget;
	QString outPiData;
	dXml::readXmlFile(&doc, outPiTarget, outPiData, fileFullPath);
	translatingToolPage->clear( );
	xmlToTranslatingStting(&doc);
	isUpdate = true;
	currentFile = fileFullPath;
	appInstances::FileBuffSystem::fileInstance.insertMapUnity(fileFullPath, doc);
}

void MenuPage::highlighted(int index) {
	QString name = listBox->itemText(index);
	fileName->setText(name);
}

const QHash<int, QString>* MenuPage::getTranslating( ) {
	if( isUpdate ) {
		isUpdate = false;
		translatingBuff = translatingToolPage->getTranslating( );
	}
	return &translatingBuff;
}
