﻿#ifndef H_MY__SINGLETONMSGSTREAM_H__H
#define	H_MY__SINGLETONMSGSTREAM_H__H
#include <qcompilerdetection.h>

namespace msg {
	class IMsgStream;
}
namespace appInstances {
	class MsgStream;
}
class appInstances::MsgStream {
	public:
		static msg::IMsgStream* instance(msg::IMsgStream* instance = Q_NULLPTR);
	};
#endif // H_MY__SINGLETONMSGSTREAM_H__H
