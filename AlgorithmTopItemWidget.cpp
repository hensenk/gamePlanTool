﻿#include "AlgorithmTopItemWidget.h"
#include <QVBoxLayout>
#include <qapplication.h>
#include <qstyle.h>
#include <QIcon>
#include "AlgorithmTopItemWidgetItem.h"
#include "XmlDataUnit.h"

widgetUI::AlgorithmTopItemWidget::AlgorithmTopItemWidget(QWidget* parent
	, const Qt::WindowFlags& f) : AAlgorithmItem{ parent,f } {
	itemLayout = new QVBoxLayout(this);
	itemLayout->addSpacerItem(new QSpacerItem(20, 40, QSizePolicy::Expanding, QSizePolicy::Expanding));
}

widgetUI::AlgorithmTopItemWidget::~AlgorithmTopItemWidget( ) {}

const QString widgetUI::AlgorithmTopItemWidget::getItemName( ) {
	static QString name = "全局公式";
	return name;
}

const dXml::XmlDataDoc widgetUI::AlgorithmTopItemWidget::getXmlData( ) const {
	dXml::XmlDataDoc doc;
	auto&& iterator = itemMap.begin( );
	auto&& end = itemMap.end( );
	for(;iterator!=end;++iterator) {
		// 遍历对象存储
		AlgorithmTopItemWidgetItem* const item = iterator.value(  );
		QString nodeName = item->checkBox->text(  );
		QString text = item->expressLabel->text(  );
		dXml::XmlDataUnit* unit = new dXml::XmlDataUnit;
		unit->setName(nodeName);
		unit->setText(text);
		doc.appendChildren(unit);
	}
	return doc;
}

QVBoxLayout* widgetUI::AlgorithmTopItemWidget::mainLayout( ) {
	return itemLayout;
}

QWidget* widgetUI::AlgorithmTopItemWidget::mainWidget( ) {
	return this;
}

const QIcon widgetUI::AlgorithmTopItemWidget::getIcon( ) {
	static QIcon icon = QApplication::style( )->standardIcon(( QStyle::StandardPixmap )30);
	return icon;
}

void widgetUI::AlgorithmTopItemWidget::addItem(const QString& name
	, const QString& express) {
	// 校验是否存在同样的公式
	if( name.isEmpty( ) || express.isEmpty( ) || itemMap.contains(name) )
		return;
	AlgorithmTopItemWidgetItem* item = new AlgorithmTopItemWidgetItem(name, express, this);
	connect(item, &AlgorithmTopItemWidgetItem::sigCheckChicked, this, &AlgorithmTopItemWidget::stateChanged);
	connect(item, &AlgorithmTopItemWidgetItem::sigPushChicked, this, &AlgorithmTopItemWidget::stateBtnChanged);
	dXml::XmlDataUnit* children = new dXml::XmlDataUnit;
	children->setName(name);
	children->setText(express);
	itemLayout->insertWidget(0, item);
	itemMap.insert(name, item);
}

void widgetUI::AlgorithmTopItemWidget::stateChanged(AlgorithmTopItemWidgetItem* item
	, int state) {
	emit pageStateChanged(item, state);
}

void widgetUI::AlgorithmTopItemWidget::stateBtnChanged(AlgorithmTopItemWidgetItem* item
	, bool state) {
	itemLayout->removeWidget(item);
	itemMap.remove(item->checkBox->text( ));
	delete item;
}
