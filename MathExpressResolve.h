﻿#ifndef H_MY__MATHEXPRESSRESOLVE_H__H
#define	H_MY__MATHEXPRESSRESOLVE_H__H
#include <QThread>
#include "dXml.h"

// 解析数学表达式
class dXml::MathExpress::MathExpressResolve : public QThread {
Q_OBJECT;
private:
	dXml::MathExpress::MathExpressBody* body;
public:
	MathExpressResolve(dXml::MathExpress::MathExpressBody* body = Q_NULLPTR
		, QObject* parent = Q_NULLPTR) : QThread{ parent },
		body{ body } {}

protected:
	void run( ) override;
public:
	void setBody(dXml::MathExpress::MathExpressBody* body) {
		this->body = body;
	}
};
#endif // H_MY__MATHEXPRESSRESOLVE_H__H
