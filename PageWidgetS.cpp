﻿#include "PageWidgetS.h"
#include "SappInstance.h"
#include "singletonPageWidgetS.h"
QHash<QString, QWidget*>& PageWidgetS::GetNameHashMap( ) {
	static QHash<QString, QWidget*> hash;
	return hash;
}


PageWidgetS::PageWidgetS( ) {}

QMutex* PageWidgetS::m_mutex( ) {
	static QMutex mutex;
	return &mutex;
}

PageWidgetS::~PageWidgetS( ) {
	QMutexLocker locker(::PageWidgetS::m_mutex( ));
	::PageWidgetS* pageInstance = appInstances::PageWidgetS::GetSigInstanceInstance( );
	if( pageInstance != Q_NULLPTR ) {
		delete pageInstance;
		pageInstance = Q_NULLPTR;
	}
}
