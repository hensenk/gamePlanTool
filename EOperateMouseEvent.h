﻿#ifndef H_MY__EOPERATEMOUSEEVENT_H__H
#define	H_MY__EOPERATEMOUSEEVENT_H__H

enum EOperateMouseEvent {
	// 按下左键
	LEFT_PRESS
	// 释放左键
	, LEFT_RELEASE
	// 移动
	, MOVE
	// 左键按下直到左键释放
	, LEFT_PRESS_TO_LEFT_RELEASE
	// 左键按下，并且移动后释放左键
	, LEFT_PRESS_TO_MOVE_TO_LEFT_RELEASE

};
#endif // H_MY__EOPERATEMOUSEEVENT_H__H
