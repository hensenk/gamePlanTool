﻿#ifndef H_MY__FILESYSTEM_H__H
#define	H_MY__FILESYSTEM_H__H
#include "SappInstance.h"

namespace appInstances {
	class FileBuffSystem;
}

class appInstances::FileBuffSystem {
	class FileInfo {
		friend class appInstances::FileBuffSystem;
	private:
		QString fullPath;
	};

private:
	FileBuffSystem( );
	// 存储当前路径
	// FileInfo* currentInfo = Q_NULLPTR;
	// 用来存储列表全路径
	QVector<QString/*全路径*/>* dirPathBody = Q_NULLPTR;

	// 存储当前路径
	QString currentDirFullPath;
	// 存储当前工作路径
	QString currentFileFullPath;
	// 存储所有文件
	QVector<QString>* filePathody;
	// 文件与 xml 解析
	QHash<QString, dXml::XmlDataDoc>* xmlMap;
public:
	const char* defaultXmlStting = ":/xml/stting.xml";
	void setCurrentDir(const QString& fullPath);
	void addFileDir(const QString& fullPath);
	void removeFileDir(const QString& fullPath);
	void clearFileDir( );
	const QString& getCurrentDir( );
	const QVector<QString> getDir(const QString& key);
	void setCurrentFilePath(const QString& fullPath);
	void addFilePath(const QString& fullPath);
	void removeFilePath(const QString& fullPath);
	void clearFilePath( );
	bool haveFileKey(const QString& filePath);
	const QString& getCurrentFilePath( );
	const QVector<QString> getFilePath(QString& key);
	void insertMapUnity(const QString& fileFullPath
		, const dXml::XmlDataDoc& xmlDocObj
		, int blend = 0);

	void removeMapUnity(const QString& key);
	void clearMapUnitys( );
	const dXml::XmlDataDoc getDocValue(const QString& key);
	const QList<QString> getKeys( );
	QHash<QString, dXml::XmlDataDoc> cloneMap( );
	static FileBuffSystem fileInstance;
	~FileBuffSystem( );
};
#endif // H_MY__FILESYSTEM_H__H
