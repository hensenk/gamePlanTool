﻿#ifndef H_MY__MENU_H__H
#define	H_MY__MENU_H__H
#include <QMenuBar>
#include "singletonMenus.h"

namespace staticMenu {
	class menus;
}

class staticMenu::menus {
public:
	friend class appInstances::Menus;
	QMenu* getMsgMeun( ) const;
	QMenu* getUserNameMenu( ) const;
	QMenu* getUserImageMenu( ) const;
	QMenu* findMenu(QString memuName) const;
	const int getMenuSize( ) const;
	int menuSetHide( );
	int menuSetHide(QStringList hideObjName);
	~menus( );
	void newQmenu(QString menuObjName, const QStringList& actions, QList<QAction*>& resultActions);
private:
	void initQmenuAction( );
	// 添加一个菜单，并且返回一个菜单，reSet 指定是否把已经存在的重新构建
	QMenu* addMenu(QString menuObjName, const QStringList& actions, QList<QAction*>& resultActions, bool reSet = false);
	QHash<QString, QMenu*> menuMap;
	menus( );
};
#endif // H_MY__MENU_H__H
