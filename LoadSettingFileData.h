﻿#ifndef H_MY__LOADSETTINGFILEDATA_H__H
#define	H_MY__LOADSETTINGFILEDATA_H__H
#include <QDomDocument>
#include <QMutexLocker>
#include <QThread>
#include "dXml.h"
#include "SappInstance.h"

namespace lThread {
	class LoadSettingFileData;
}


class lThread::LoadSettingFileData : public QThread {
Q_OBJECT;
private:
	friend appInstances::LoadSettingFileData;
	int* threadRunStatus(int* status = Q_NULLPTR);
	void releaseResources( );
	const SigInstance* m_sigInstance = Q_NULLPTR;
public:
	LoadSettingFileData( const SigInstance* sigInstance, QWidget* paren);
	~LoadSettingFileData( );
	void stopTask( );
	void startTask( );

	// 任务类型
	enum LoadSettingType {
		NONE
		, READ
		, WRITE
	};

	/*
	 * 设置任务
	 * taskType 任务类型
	 * path 文件目录
	 * obj 读写对象，该对象不会由线程维护
	 */
	void setTask(LoadSettingType taskType, QString path, dXml::XmlDataUnit* obj);
protected:
	void runReadTask(dXml::XmlDataUnit* saveToData, QString& outPITarget, QString& outPIData, const QString& file);
	void runWriteTask(dXml::XmlDataUnit* saveToData, const QString& file);
	void run( ) override;
private:
	LoadSettingType taskType;
	QString taskPath;
	QString taskDir;
	dXml::XmlDataUnit* taskDoc;
	QMutex* q_mutex = Q_NULLPTR;
signals:
	// 读任务完成
	void readOverTask(dXml::XmlDataUnit* taskDoc);
	// 写任务完成
	void writeOverTask(dXml::XmlDataUnit* taskDoc);
};
#endif // H_MY__LOADSETTINGFILEDATA_H__H
