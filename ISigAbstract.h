﻿#ifndef H_MY__ISIGABSTRACT_H__H
#define	H_MY__ISIGABSTRACT_H__H
#include <QObject>

class ISigAbstarct : public QObject {
Q_OBJECT;
protected:
	ISigAbstarct( ) {
	};

public:
	virtual ~ISigAbstarct( ) {
	}

	virtual void appMsgMessage(const QString& msg, QString appChars ) = 0;
	virtual void appMsgMessage(const QStringList& msg, QString appChars ) = 0;
	virtual void appMsgMessage(const QList<QString>& msg, QString appChars ) = 0;
signals:
	void appMsg(const QString& msg, QString appChars );
	void appMsg(const QStringList& msg, QString appChars );
	void appMsg(const QList<QString>& msg, QString appChars );
};
#endif // H_MY__ISIGABSTRACT_H__H
