﻿#ifndef H_MY__LOCALSETTING_H__H
#define	H_MY__LOCALSETTING_H__H
#include <QWidget>

class LoadThreadSettingToolBoxPage : public QWidget {
	Q_OBJECT;
public:
	LoadThreadSettingToolBoxPage(QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
};
#endif // H_MY__LOCALSETTING_H__H
