﻿#include "singletonMenus.h"
#include "menus.h"

staticMenu::menus* appInstances::Menus::GetSigInstanceInstance(bool deleteFlag) {
	static staticMenu::menus* instance = Q_NULLPTR;
	if( deleteFlag == true && instance != Q_NULLPTR ) {
		delete instance;
		instance = Q_NULLPTR;
		return instance;
	}
	if( instance == Q_NULLPTR && !deleteFlag ) {
		instance = new staticMenu::menus( );
	}
	return instance;
}
