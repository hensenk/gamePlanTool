﻿#include "CommShowMsgItem.h"
#include <qstyle.h>
#include <QHBoxLayout>
#include <QDir>
#include <QEvent>
#include <qmenu.h>
#include <QMouseEvent>
#include <QPoint>
#include <QAction>
#include <qdebug.h>
#include <qtextbrowser.h>
#include "IMsgStream.h"
#include <QApplication>
#include <QClipboard>

void CommShowMsgItem::setBorwseStatus(QTextBrowser* pTextBrowser) {
	pTextBrowser->setLineWrapMode(QTextEdit::WidgetWidth);
	pTextBrowser->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	pTextBrowser->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void CommShowMsgItem::setLabelStatus(QLabel* label) {
	label->setWordWrap(true);
	label->setIndent(8);
	label->setTextInteractionFlags(label->textInteractionFlags( ) | Qt::TextSelectableByMouse);
}

CommShowMsgItem::CommShowMsgItem(msgType pmsgBodyType, QString& puserName, QString& pmsg, QString& pimagePath, QWidget* pparent) : QWidget(pparent) {
	// ui.setupUi(this);
	this->msgBodyType = pmsgBodyType;
	this->userName = puserName;
	this->msg = pmsg;
	this->imagePath = pimagePath;
	luserMsg = new QLabel(this);
	luserName = new QLabel(this);
	QFont font;
	font.setPointSize(32);
	luserName->setFont(font);
	//设置颜色
	QPalette pa;
	pa.setColor(QPalette::WindowText, Qt::red);
	luserName->setPalette(pa);
	luserName->setText(puserName);
	userImage = new QLabel(this);
	setLabelStatus(luserMsg);
	QSize picSize(128, 128);
	QPixmap pixmap = QPixmap(pimagePath).scaled(picSize, Qt::KeepAspectRatio);
	userImage->setPixmap(pixmap);
	luserMsg->setText(pmsg);
	mainLayout = new QVBoxLayout( );
	setLayout(mainLayout);
	QHBoxLayout* userInfo = new QHBoxLayout( );
	hSpacar = new QSpacerItem(40, 20, QSizePolicy::Expanding);
	if( pmsgBodyType == msgType::ACCEPT ) {
		userInfo->addWidget(userImage);
		userInfo->addWidget(luserName);
		userInfo->addSpacerItem(hSpacar);
	} else if( pmsgBodyType == msgType::SENDER ) {
		userInfo->addSpacerItem(hSpacar);
		userInfo->addWidget(luserName);
		userInfo->addWidget(userImage);
	}
	mainLayout->addLayout(userInfo);
	mainLayout->addWidget(luserMsg);
	// 设置对象名称
	setObjectName(itemName::CommShowMsgItemObjName::getItemObjName( ));
	luserMsg->setObjectName(itemName::CommShowMsgItemObjName::getMsgLabelObjName( ));
	luserName->setObjectName(itemName::CommShowMsgItemObjName::getUserNameObjName( ));
	userImage->setObjectName(itemName::CommShowMsgItemObjName::getUserImageObjName( ));
}

CommShowMsgItem::~CommShowMsgItem( ) {}
