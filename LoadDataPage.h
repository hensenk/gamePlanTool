﻿#pragma once
#include <QWidget>
#include "ui_LoadDataPage.h"
#include "ATabPageBase.h"
#include <QStandardItemModel>
#include <QStandardItem>
#include <QTreeView>
#include <QTreeWidgetItem>
#include <QFileInfo>

namespace widgetUI {
	class LoadDataPage;
}

class widgetUI::LoadDataPage : public ATabPageBase {
Q_OBJECT;
public:
	void activation(QObject* obj, int index, EVENTFLAG status) override;
	void initControl( );

	typedef enum {
		GETFLAG = 0x0
		, UPDATEBTNSIGNALS = 0x1
		, MENUBTNSIGNALS = 0x2
		, UPDIRBTNSIGNALS = 0x4
		, CURRENTDIRBTNSIGNALS = 0x8
		, GLOBALBTNSIGNALS=0x10
		, STOPUPDATEBTNSIGNALS=0x20
		, ALLSIGNALS = 0x3f
		, CANCEL = -1
	} LOADDATAPAGESIGNALSFLAG;

	using LoadFlag = LOADDATAPAGESIGNALSFLAG;
	LoadFlag addBtnSignals(LoadFlag flag);
	LoadFlag subBtnSignals(LoadFlag flag);
	LoadFlag clearBtnSignals(LoadFlag flag);
	// 设置信号
	LoadFlag setBtnSignals(LoadFlag flag = GETFLAG);
	LoadFlag setBtnSignals(int flag);
public:
	LoadDataPage(QWidget* parent = Q_NULLPTR);
	void initTreeView( );
	void chenageVValue(int position);
	~LoadDataPage( );
QLineEdit* getFilePath( );
QTreeWidget* getWidgetTreeWidget();
private:
	Ui::LoadDataPage ui;
	QTreeWidgetItem* rootItem;
public:

	/*static LoadDataPage* getInstance( );*/
	QString getName( ) override;
	QIcon getIcon( ) override;
	static QProgressBar* Progress(QProgressBar* progressBar = Q_NULLPTR, bool forceSet = false);
private: // 触发信号 
	void sendSignal(bool status);
signals: // 窗口触发
	void currentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous);
signals: // 按钮信号
	// 按钮全局
	void onClick(QWidget* mainWidget, QPushButton* controlObj, bool status, QTreeWidget* treeWidget, QLineEdit* filePath);
	// 更新
	void updateBtn(QWidget* mainWidget, QPushButton* controlObj, bool status, QTreeWidget* treeWidget, QLineEdit* filePath);
	// 菜单
	void menuBtn(QWidget* mainWidget, QPushButton* controlObj, bool status, QTreeWidget* treeWidget, QLineEdit* filePath);
	// 上一级
	void upDirBtn(QWidget* mainWidget, QPushButton* controlObj, bool status, QTreeWidget* treeWidget, QLineEdit* filePath);
	// 程序目录
	void resetPathDirBtn(QWidget* mainWidget, QPushButton* controlObj, bool status, QTreeWidget* treeWidget, QLineEdit* filePath);
	// 停止更新
	void stopUpdateBtn(QWidget* mainWidget, QPushButton* controlObj, bool status, QTreeWidget* treeWidget, QLineEdit* filePath);
public: // 测试信号
	// void treeItemExpanded(QTreeWidgetItem* item);
};
