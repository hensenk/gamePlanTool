# gamePlanTool

#### 介绍
qt -- 泛用性策划工具

#

#### 使用说明
创建人物    
![创建人物 ](https://images.gitee.com/uploads/images/2020/0302/092342_5a69da6b_3041587.gif "创建人物.gif")    
修改属性值（值支持数字）    
![修改属性值](https://images.gitee.com/uploads/images/2020/0302/092527_83e078fe_3041587.gif "修改属性值.gif")    
值的翻译，需要保存才能存储最新翻译，同步只用于菜单页面的信息同步    
![翻译](https://images.gitee.com/uploads/images/2020/0302/092821_1d1eacd9_3041587.gif "翻译.gif")    
#### 可以参考目录下的 xml 配置文件，该软件导出信息为 xml    
#### 支持安卓平台    
衍生版本会导致路径访问与权限不一致（布局暂时没有修正）   
![安卓演示](https://images.gitee.com/uploads/images/2020/0302/095811_074cde98_3041587.gif "安卓演示4.gif")