﻿#include "TranslatingToolPage.h"
#include <QtCore/QtCore>
#include <XmlDataUnit.h>
#include <qicon.h>
#include <qstring.h>
#include <qobject.h>
#include <qapplication.h>
#include <qstyle.h>
#include <QFocusEvent>

void widgetUI::TranslatingItem::inintConnect( ) {
	connect(this->btn, &QPushButton::clicked, this, &widgetUI::TranslatingItem::onBtnClicked);
	connect(this->edit, &QLineEdit::textChanged, this, &widgetUI::TranslatingItem::lineEditChaned);
	connect(this->edit, &widgetUI::TranslatingLineEdit::LineEditFocusInEvent, this, &widgetUI::TranslatingItem::LineEditFocusInEvent);
	connect(this->edit, &widgetUI::TranslatingLineEdit::LineEditFocusOutEvent, this, &widgetUI::TranslatingItem::LineEditFocusOutEvent);
}

widgetUI::TranslatingItem::TranslatingItem(QWidget* parent, const Qt::WindowFlags& f) : QWidget(parent, f) {
	this->edit = new TranslatingLineEdit(this);
	this->btn = new QPushButton(this);
	this->mainLayout = new QHBoxLayout;
	setLayout(mainLayout);
	mainLayout->addWidget(btn);
	mainLayout->addWidget(edit);
	inintConnect( );
}

widgetUI::TranslatingItem::TranslatingItem(QPushButton* btn, TranslatingLineEdit* edit, QWidget* parent, const Qt::WindowFlags& f): QWidget(parent, f) {
	this->mainLayout = new QHBoxLayout;
	setLayout(mainLayout);
	mainLayout->addWidget(btn);
	if( edit == Q_NULLPTR )
		edit = new TranslatingLineEdit(this);
	mainLayout->addWidget(edit);
	this->btn = btn;
	this->edit = edit;
	inintConnect( );
}

widgetUI::TranslatingItem::TranslatingItem(const QString& btnName, const QString& content, QWidget* parent) : QWidget(parent) {
	this->edit = new TranslatingLineEdit(this);
	this->btn = new QPushButton(this);
	this->edit->setText(content);
	this->btn->setText(btnName);
	//TranslatingItem(button, lineEdit, paren, Qt::WindowFlags( ));
	this->mainLayout = new QHBoxLayout;
	setLayout(mainLayout);
	mainLayout->addWidget(btn, 1);
	mainLayout->addWidget(edit, 3);
	inintConnect( );
}

void widgetUI::TranslatingItem::onBtnClicked(bool clicked) {
	emit this->btnOnClicked(this->edit, this->btn, clicked);
}

void widgetUI::TranslatingItem::lineEditChaned(const QString& text) {
	emit this->translatingLineEditChaned(this, text);
}

void widgetUI::TranslatingLineEdit::focusInEvent(QFocusEvent* event) {
	QLineEdit::focusInEvent(event);
	emit this->LineEditFocusInEvent(this, event);
}

void widgetUI::TranslatingLineEdit::focusOutEvent(QFocusEvent* event) {
	QLineEdit::focusOutEvent(event);
	emit this->LineEditFocusOutEvent(this, event);
}

widgetUI::TranslatingToolPage::TranslatingToolPage(QWidget* paren) {
	// 设置图标
	QIcon icon = QApplication::style( )->standardIcon(( QStyle::StandardPixmap )30);
	// 新建节点
	xmlUnit = new dXml::XmlDataUnit;
	mainLayout = new QVBoxLayout;
	translatingSetting = new QList<TranslatingItem*>;
	tMapXmlNodeSetting = new QHash<int, dXml::XmlDataUnit*>;
	tMapXmlTranslating = new QHash<int, QString>;
	setLayout(mainLayout);
	spacerItem = new QSpacerItem(20, 40, QSizePolicy::Maximum, QSizePolicy::Expanding);
	mainLayout->addItem(spacerItem);
}

widgetUI::TranslatingToolPage::~TranslatingToolPage( ) {
	delete tMapXmlNodeSetting;
	delete translatingSetting;
	delete tMapXmlTranslating;
	delete xmlUnit;
}

void widgetUI::TranslatingToolPage::activation(QObject* obj, int index) {}

QString widgetUI::TranslatingToolPage::getName( ) {
	return objPageName;
}

void widgetUI::TranslatingToolPage::setName(QString& name) {
	objPageName = name;
}

QIcon widgetUI::TranslatingToolPage::getIcon( ) {
	return objPageIcon;
}

void widgetUI::TranslatingToolPage::setIcon(QIcon& icon) {
	objPageIcon = icon;
}

void widgetUI::TranslatingToolPage::setTitle(QString& str) {}

dXml::XmlDataUnit widgetUI::TranslatingToolPage::getXmlUnityToCloneObj( ) {
	return *xmlUnit;
}

bool widgetUI::TranslatingToolPage::synchXmlUnit( ) {
	QString name = xmlUnit->nodeName( );
	clear( );
	xmlUnit->setName(name);
	QList<TranslatingItem*>::iterator iterator = translatingSetting->begin( );
	QList<TranslatingItem*>::iterator end = translatingSetting->end( );
	for( ;iterator != end;++iterator ) {
		TranslatingItem* translatingItem = *iterator;
		auto NodeName = "node_" + translatingItem->btn->text( );
		QString text = translatingItem->edit->text( );
		dXml::XmlDataUnit* unit = new dXml::XmlDataUnit;
		unit->setName(NodeName);
		unit->setText(text);
		xmlUnit->appendChildren(unit);
	}
	return true;
}

void widgetUI::TranslatingToolPage::objConnectSig(widgetUI::TranslatingItem* item) {
	connect(item, &TranslatingItem::btnOnClicked, this, &TranslatingToolPage::saveClicked, Qt::UniqueConnection);
	connect(item, &TranslatingItem::translatingLineEditChaned, this, &TranslatingToolPage::lineEditChaned, Qt::UniqueConnection);
	connect(item, &TranslatingItem::LineEditFocusInEvent, this, &TranslatingToolPage::LineEditFocusInEvent, Qt::UniqueConnection);
	connect(item, &TranslatingItem::LineEditFocusOutEvent, this, &TranslatingToolPage::LineEditFocusOutEvent, Qt::UniqueConnection);
}

void widgetUI::TranslatingToolPage::appItems(QList<dXml::XmlDataUnit*>* nodes, dXml::XmlDataUnit* paren) {
	QList<dXml::XmlDataUnit*>::iterator iterator = nodes->begin( );
	QList<dXml::XmlDataUnit*>::iterator end = nodes->end( );
	for( ;iterator != end;++iterator ) {
		dXml::XmlDataUnit* unit = *iterator;
		paren->appendChildren(unit);
		QString nodeName = unit->nodeName( );
		nodeName = nodeName.remove("node_", Qt::CaseInsensitive);
		QString text = unit->getText( );
		TranslatingItem* item = new TranslatingItem(nodeName, text, this);
		int akey = nodeName.toInt( );
		tMapXmlNodeSetting->insert(akey, unit);
		tMapXmlTranslating->insert(akey, text);
		translatingSetting->append(item);
		int index = this->children( ).length( ) - 2;
		if( index < 0 )
			index = 0;
		mainLayout->insertWidget(index, item);
		objConnectSig(item);
	}
}

void widgetUI::TranslatingToolPage::saveClicked(TranslatingLineEdit* editBar, QPushButton* chickedObj, bool chicked) {
	QString btnText = chickedObj->text( );
	if( ! btnText.endsWith('*') )
		return;
	btnText = btnText.split('*')[0];
	chickedObj->setText(btnText);
	int akey = btnText.toInt( );
	QString text = editBar->text( );
	tMapXmlTranslating->insert(akey, text);
	tMapXmlNodeSetting->value(akey)->setText(text);
}

void widgetUI::TranslatingToolPage::lineEditChaned(const TranslatingItem* item, const QString& text) {
	QString btnText = item->btn->text( );
	if( !btnText.endsWith('*') ) {
		item->btn->setText(btnText.append('*'));
	}
}

void widgetUI::TranslatingToolPage::LineEditFocusInEvent(TranslatingLineEdit* EventObj, QFocusEvent* event) {}

void widgetUI::TranslatingToolPage::LineEditFocusOutEvent(TranslatingLineEdit* EventObj, QFocusEvent* event) {}

void widgetUI::TranslatingToolPage::translatingChanged(QString& text) {}

const QHash<int, QString> widgetUI::TranslatingToolPage::getTranslating( ) {
	QHash<int, QString> result = *this->tMapXmlTranslating;
	return result;
}

dXml::XmlDataDoc widgetUI::TranslatingToolPage::getItemsToDoc( ) {
	const QList<QObject*> list = this->children( );
	dXml::XmlDataUnit* rootUnit = new dXml::XmlDataUnit(objPageName);
	for( auto& obj : list ) {
		TranslatingItem* item = qobject_cast<TranslatingItem*>(obj);
		if( item == Q_NULLPTR )
			continue;
		emit item->btnOnClicked(item->edit, item->btn, true);
		QString nodeName = "node_" + item->btn->text( );
		auto&& text = item->edit->text( );
		dXml::XmlDataUnit* unit = new dXml::XmlDataUnit(nodeName);
		unit->setText(text);
		rootUnit->appendChildren(unit);
	}
	dXml::XmlDataDoc doc;
	doc.appendChildren(rootUnit);
	return doc;
}

void widgetUI::TranslatingToolPage::sortWidget(bool order) {
	if( order ) {
		qSort(translatingSetting->begin( ), translatingSetting->end( ), &TranslatingToolPage::sortOrder);
		xmlUnit->sort([=](dXml::XmlDataUnit* left,dXml::XmlDataUnit* right) {
			int leftInt = left->nodeName( ).remove("node_").toInt( );
			int rightInt = right->nodeName( ).remove("node_").toInt( );
			if( leftInt > rightInt )
				return false;
			return true;
		});
	} else {
		qSort(translatingSetting->begin( ), translatingSetting->end( ), &TranslatingToolPage::sortReversingOrder);
		xmlUnit->sort([=](dXml::XmlDataUnit* left,dXml::XmlDataUnit* right) {
			int leftInt = left->nodeName( ).remove("node_").toInt( );
			int rightInt = right->nodeName( ).remove("node_").toInt( );
			if( leftInt < rightInt )
				return false;
			return true;
		});
	}
	const QList<QObject*> objects = this->children( );
	QList<QObject*>::const_iterator objectsbegin = objects.begin( );
	QList<QObject*>::const_iterator objectsend = objects.end( );
	QList<QObject*> removeObjList;
	for( ;objectsbegin != objectsend;++objectsbegin ) {
		if( ( *objectsbegin )->inherits("TranslatingItem") )
			removeObjList.append(*objectsbegin);
	}
	objectsbegin = removeObjList.begin( );
	objectsend = removeObjList.end( );
	for( ;objectsbegin != objectsend;++objectsbegin ) {
		QObject* object = *objectsbegin;
		if( !object->inherits("QSpacerItem") )
			mainLayout->removeWidget(qobject_cast<QWidget*>(object));
	}
	QList<TranslatingItem*>::iterator translatingSettingiterator = translatingSetting->begin( );
	QList<TranslatingItem*>::iterator translatingSettingend = translatingSetting->end( );
	for( ;translatingSettingiterator != translatingSettingend;++translatingSettingiterator ) {
		mainLayout->insertWidget(0, *translatingSettingiterator);
	}
}

void widgetUI::TranslatingToolPage::clear( ) {
	while( !translatingSetting->isEmpty( ) ) {
		delete translatingSetting->last( );
		translatingSetting->removeLast( );
	}
	tMapXmlNodeSetting->clear( );
	tMapXmlTranslating->clear( );
	xmlUnit->clear( );
	const QList<QObject*> objects = this->children( );
	for( auto& obj : objects ) {
		if( !obj->inherits("QSpacerItem") ) {
			TranslatingItem* widget = qobject_cast<TranslatingItem*>(obj);
			mainLayout->removeWidget(widget);
			delete widget;
		}
	}
}

bool widgetUI::TranslatingToolPage::sortOrder(TranslatingItem* barAmount1, TranslatingItem* barAmount2) {
	QString btn1Text = barAmount1->btn->text( );
	int left = btn1Text.toInt( );
	QString btn2Text = barAmount2->btn->text( );
	int right = btn2Text.toInt( );
	if( left < right )
		return true;
	return false;
}

bool widgetUI::TranslatingToolPage::sortReversingOrder(TranslatingItem* barAmount1, TranslatingItem* barAmount2) {
	int left = barAmount1->btn->text( ).toInt( );
	int right = barAmount2->btn->text( ).toInt( );
	if( left > right )
		return true;
	return false;
}

QList<QWidget*> widgetUI::TranslatingToolPage::setXmlUnityDom(dXml::XmlDataUnit* mode) {
	QList<QWidget*> result;
	xmlUnit->appendAttrs(*mode->getAttrs( ));
	appItems(mode->getChildrens( ), this->xmlUnit);
	return result;
}

QList<QWidget*> widgetUI::TranslatingToolPage::setXmlUnityDoms(QList<dXml::XmlDataUnit*>& modes) {
	QList<QWidget*> result;
	if( modes.isEmpty( ) )
		return result;
	xmlUnit->clear( );
	xmlUnit->setName(modes.last( )->nodeName( ));
	QList<dXml::XmlDataUnit*>::iterator iterator = modes.begin( );
	QList<dXml::XmlDataUnit*>::iterator end = modes.end( );
	for( ;iterator != end;++iterator ) {
		dXml::XmlDataUnit* unit = *iterator;
		result.append(setXmlUnityDom(unit));
	}
	return result;
}

QWidget* widgetUI::TranslatingToolPage::addItem(QIcon icon, int nodeCode, const QString& Data, QHash<QString, QString>* attrMap) {
	QString name = QString(nodeCode);
	TranslatingItem* item = new TranslatingItem(name, Data, this);
	int index = this->children( ).length( ) - 2;
	if( index < 0 )
		index = 0;
	mainLayout->insertWidget(index, item);
	item->btn->setIcon(icon);
	translatingSetting->append(item);
	name.insert(0, "node_");
	dXml::XmlDataUnit* xmlDataUnit = new dXml::XmlDataUnit(name);
	xmlDataUnit->setText(Data);
	if( attrMap != Q_NULLPTR && !attrMap->isEmpty( ) )
		xmlDataUnit->appendAttrs(*attrMap);
	xmlUnit->appendChildren(xmlDataUnit);
	tMapXmlNodeSetting->insert(nodeCode, xmlDataUnit);
	tMapXmlTranslating->insert(nodeCode, Data);
	objConnectSig(item);
	return item;
}

QWidget* widgetUI::TranslatingToolPage::addItem(int nodeCode, const QString& Data, QHash<QString, QString>* attrMap) {
	QString name = QString("%1").arg(nodeCode);
	TranslatingItem* item = new TranslatingItem(name, Data, this);
	int index = this->children( ).length( ) - 2;
	if( index < 0 )
		index = 0;
	mainLayout->insertWidget(index, item);
	translatingSetting->append(item);
	name.insert(0, "node_");
	dXml::XmlDataUnit* xmlDataUnit = new dXml::XmlDataUnit(name);
	xmlDataUnit->setText(Data);
	if( attrMap != Q_NULLPTR && !attrMap->isEmpty( ) )
		xmlDataUnit->appendAttrs(*attrMap);
	xmlUnit->appendChildren(xmlDataUnit);
	tMapXmlNodeSetting->insert(nodeCode, xmlDataUnit);
	tMapXmlTranslating->insert(nodeCode, Data);
	objConnectSig(item);
	return item;
}
