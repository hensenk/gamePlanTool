﻿#include "menus.h"
#include <QHash>
#include <QAction>

/*
 *
	QMenu* msgMeun;
	QMenu* userNameMenu;
	QMenu* userImageMenu;
 * 
 */
QMenu* staticMenu::menus::getMsgMeun( ) const {
	return this->findMenu("msgMeun");
}

QMenu* staticMenu::menus::getUserNameMenu( ) const {
	return this->findMenu("userNameMenu");
}

QMenu* staticMenu::menus::getUserImageMenu( ) const {
	return this->findMenu("userImageMenu");
}

QMenu* staticMenu::menus::findMenu(QString memuName) const {
	QHash<QString, QMenu*>::ConstIterator iterator = menuMap.find(memuName);
	if( iterator == menuMap.end( ) )
		return Q_NULLPTR;
	QMenu* const menu = iterator.value( );
	return menu;
}

const int staticMenu::menus::getMenuSize( ) const {
	return menuMap.size( );
}

int staticMenu::menus::menuSetHide(QStringList hideObjName) {
	int result = 0;
	for( auto& meun : menuMap ) {
		bool isHidden = meun->isHidden( );
		bool actionVisible = meun->menuAction( )->isVisible( );
		if( !isHidden && actionVisible )
			continue;
		int size = hideObjName.size( );
		int i = 0;
		for( ;i < size;++i ) {
			if( hideObjName[i] == meun->objectName( ) ) {
				meun->menuAction( )->setVisible(false);
				meun->hide( );
				result++;
			}
		}
		hideObjName.removeAt(i);
		if( size - 1 < 0 )
			return result;
	}
	return result;
}

int staticMenu::menus::menuSetHide( ) {
	int result = 0;
	for( auto& meun : menuMap ) {
		bool isHidden = meun->isHidden( );
		bool actionVisible = meun->menuAction( )->isVisible( );
		if( !isHidden && actionVisible )
			continue;
		meun->menuAction( )->setVisible(false);
		meun->hide( );
		result++;
	}
	return result;
}

staticMenu::menus::~menus( ) {
	for( auto iterator = menuMap.begin( );iterator != menuMap.end( );++iterator ) {
		auto& menu = iterator.value( );
		delete menu;
	}
}

void staticMenu::menus::newQmenu(QString menuObjName, const QStringList& actions, QList<QAction*>& resultActions) {
	addMenu(menuObjName, actions, resultActions);
}

void staticMenu::menus::initQmenuAction( ) {}

QMenu* staticMenu::menus::addMenu(QString menuObjName, const QStringList& actions, QList<QAction*>& resultActions, bool reSet) {
	QHash<QString, QMenu*>::iterator menuIterator = menuMap.find(menuObjName);
	QMenu* menu = menuIterator.value( );
	if( menuIterator != menuMap.end( ) && !reSet )
		return menu;
	menu = new QMenu( );
	menu->setObjectName(menuObjName);
	menuMap.insert(menuObjName, menu);
	if( !actions.isEmpty( ) ) {
		for( auto& actionName : actions )
			resultActions.append(menu->addAction(actionName));
	}
	return menu;
}

staticMenu::menus::menus( ) {
	QList<QAction*> resultActions = QList<QAction*>( );
	QStringList actions = QStringList( );
	actions << "消息";
	newQmenu("msgMeun", actions, resultActions);
	actions.clear( );
	actions << "用户名称";
	newQmenu("userNameMenu", actions, resultActions);
	actions.clear( );
	actions << "用户图片";
	newQmenu("userImageMenu", actions, resultActions);
	initQmenuAction( );
}
