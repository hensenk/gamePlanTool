﻿#include "DxmlSttringMap.h"

appInstances::DxmlSttringMap::DxmlSttringMap( ) {
	this->map = new QHash<QString, dXml::XmlDataDoc*>;
	defaultDoc = new dXml::XmlDataDoc;
	defaultDoc->setName("setting");
}

appInstances::DxmlSttringMap* appInstances::DxmlSttringMap::getInstance( ) {
	static DxmlSttringMap stance;
	return &stance;
}

appInstances::DxmlSttringMap::~DxmlSttringMap( ) {
	QList<dXml::XmlDataDoc*> list = map->values( );
	while( !list.isEmpty( ) ) {
		delete list.last( );
		list.removeLast( );
	}
	delete map;
	delete defaultDoc;
}

appInstances::DxmlSttringMap::xmlPrit appInstances::DxmlSttringMap::getXmlFile(QString& fileName) {
	dXml::XmlDataDoc* const value = map->value(fileName);
	return xmlPrit(fileName, value);
}

const dXml::XmlDataDoc* appInstances::DxmlSttringMap::getDefaultDocXmlFile( ) {
	return defaultDoc;
}

appInstances::DxmlSttringMap::xmlPrit appInstances::DxmlSttringMap::addXmlFile(QString& fileName) {
	dXml::XmlDataDoc* value = map->value(fileName);
	if( !value ) {
		value = new dXml::XmlDataDoc;
	}
	value->clear( );
	map->insert(fileName, value);
	return xmlPrit(fileName, value);
}
