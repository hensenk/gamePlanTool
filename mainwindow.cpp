﻿#include "mainwindow.h"
#include <QApplication>
#include <qscreen.h>
#include <qwindow.h>
#include <QSharedMemory>
#include "MyApplication.h"
#include <qhash.h>
#include <qobject.h>
#include "SigInstance.h"
#include <qevent.h>
#if defined(Q_OS_ANDROID)
#include <QtAndroid>
#endif
using namespace widgetUI;

void MainWindow::setFileAuthor(const QString& iconPath
	, const QString& authorName) {
	fileUserIcon->setPixmap(QPixmap(iconPath).scaled(32, 32, Qt::KeepAspectRatio));
	fileUserName->setText(authorName);
}

// 初始化主要界面
void MainWindow::initAppControl( ) {
	mainLayout = new QHBoxLayout;
	mainWidget = new QWidget(this);
	mainWidget->setLayout(mainLayout);
	this->setCentralWidget(mainWidget);
	creatorPersonBtn = new QPushButton("创建人物档案", this);
	person = new QLineEdit(this);
	person->setPlaceholderText("输入新建人物档案名称");
	objListWidget = new QListWidget( );
	personLayout = new QVBoxLayout( );
	personLayout->addWidget(person);
	personLayout->addWidget(objListWidget);
	personLayout->addWidget(creatorPersonBtn);
	fileUserIcon = new QLabel( );
	fileUserName = new QLabel( );
	QHBoxLayout* userInfoLayout = new QHBoxLayout( );
	setFileAuthor(QString(":/image/waring.png"), QString("匿名"));
	userInfoLayout->addWidget(fileUserIcon, 2, Qt::AlignLeft);
	userInfoLayout->addWidget(new QLabel("作者:"), 2, Qt::AlignCenter);
	userInfoLayout->addWidget(fileUserName, 4, Qt::AlignCenter);
	userInfoLayout->setContentsMargins(0, 0, 0, 0);
	userInfoLayout->setSpacing(0);
	QVBoxLayout* fileInfo = new QVBoxLayout( );
	fileInfo->addLayout(userInfoLayout, 1);
	fileInfo->addLayout(personLayout, 9);
	fileInfo->setContentsMargins(0, 0, 0, 0);
	fileInfo->setSpacing(0);
	mainLayout->addLayout(fileInfo, 1);
	tabWidget = new QTabWidget(mainWidget);
	mainLayout->addWidget(tabWidget, 4);
	tabWidget->setMovable(false);
	status_bar = statusBar( );
	editStatus = new QLabel("-> 不可编辑");
	spacer = new QLabel(editStatus);
	editStatus->setTextFormat(Qt::PlainText);
	editStatus->setFrameShadow(QFrame::Raised);
	editStatus->setFrameShape(QFrame::WinPanel);
	status_bar->addPermanentWidget(spacer, 8);
	spacer->setEnabled(false);
	status_bar->addPermanentWidget(editStatus, 2);
	editStatus->setObjectName("editStatus");
	editStatus->installEventFilter(this);
	msgPage = new widgetUI::MsgPage(this);
	msgPageInstance(msgPage, true);
	loadDataPage = new widgetUI::LoadDataPage(this);
	loadDataPageInstance(loadDataPage, true);
	propertyPage = new widgetUI::PropertyPage(this);
	propertyPageInstance(propertyPage, true);
	algorithmPage = new widgetUI::AlgorithmPage(this);
	algorithmInstance(algorithmPage, true);
	commPage = new widgetUI::CommunicationPage(this);
	commPageInstance(commPage, true);
	menuPage = new widgetUI::MenuPage(this);
	menuPageInstance(menuPage, true);
	addPage(loadDataPage);
	addPage(propertyPage);
	addPage(algorithmPage);
	addPage(commPage);
	addPage(menuPage);
	addPage(msgPage);
}

void MainWindow::initPermissing( ) {
#if defined(Q_OS_ANDROID)
    QStringList permissions;
    permissions<< "android.permission.READ_EXTERNAL_STORAGE" << "android.permission.WRITE_SETTINGS" << "android.permission.WRITE_EXTERNAL_STORAGE";
    for(auto& permission : permissions){
        QtAndroid::PermissionResult r = QtAndroid::checkPermission(permission);
        if(r != QtAndroid::PermissionResult::Granted) {
            QtAndroid::requestPermissionsSync( QStringList() << permission );
            r = QtAndroid::checkPermission(permission);
        }
    }
#endif
}

void MainWindow::initStatusMenu(QMenu& status_edit_menu) {
	QAction* action = status_edit_menu.addAction("状态");
	action->setCheckable(true);
	connect(action, &QAction::triggered, this, &MainWindow::statusEditTrigger);
}

inline void widgetUI::MainWindow::statusEditTrigger(bool checked) {
	QAction* action = qobject_cast<QAction*>(sender( ));
	if( action->isChecked( ) )
		this->editStatus->setText("-> 可编辑");
	else
		this->editStatus->setText("-> 不可编辑");
}

bool MainWindow::eventFilter(QObject* watched
	, QEvent* event) {
	static QMenu statusEditMenu;
	static bool isInit = false;
	if( !isInit ) {
		initStatusMenu(statusEditMenu);
		isInit = true;
	}
	/*QWidget* widget_at = QApplication::widgetAt(QCursor::pos( ));
	if( widget_at )
		qDebug( ) << "widget_at:\t" << widget_at->objectName( ) << "watched:\t" << watched->objectName( );*/
	if( watched->objectName( ) != editStatus->objectName( ) ) {
		return QWidget::eventFilter(watched, event);
	}
	QEvent::Type type = event->type( );
	if( type == QEvent::MouseButtonRelease ) {
		//	statusEditMenu.show( );
		QMouseEvent* mouse_event = dynamic_cast<QMouseEvent*>( event );
		statusEditMenu.show( );
		QSize size = statusEditMenu.size( );
		QPoint mouseGPos = QCursor::pos( );
		QPoint pos(mouseGPos.x( ) - size.width( ), mouseGPos.y( ) - size.height( ));
		statusEditMenu.move(pos);
	}
	return QWidget::eventFilter(watched, event);
}

int MainWindow::addPage(ATabPageBase* page) {
	return tabWidget->addTab(page, page->getIcon( ), page->getName( ));
}

void MainWindow::initConnection( ) {
	// page槽关联
	connect(tabWidget, &QTabWidget::currentChanged, this, &widgetUI::MainWindow::currentChanged);
	connect(tabWidget, &QTabWidget::tabBarClicked, this, &widgetUI::MainWindow::tabBarClicked);
	connect(tabWidget, &QTabWidget::tabBarDoubleClicked, this, &widgetUI::MainWindow::tabBarDoubleClicked);
	connect(objListWidget, &QListWidget::itemClicked, this, &widgetUI::MainWindow::objListItemClicked);
	connect(creatorPersonBtn, &QPushButton::clicked, this, &widgetUI::MainWindow::creatorPersonClick);
}

void MainWindow::toolStatusBarClick( ) {}

void MainWindow::creatorPersonClick(bool clicked) {
	emit creatorPerson(this, qobject_cast<QPushButton*>(sender( )), clicked);
}

void MainWindow::objListItemClicked(QListWidgetItem* item) {
	emit listItemClicked(dynamic_cast<MyListWidgetItem*>( item ));
}

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent) {
#if defined(Q_OS_ANDROID)
    initPermissing();
#endif
	initAppControl( );
	initConnection( );
}

MainWindow::~MainWindow( ) {
	
}

void MainWindow::clearList( ) {
	objListWidget->clear( );
}

void MainWindow::additemToList(MyListWidgetItem* item) {
	objListWidget->addItem(item);
}

void MainWindow::currentChanged(int index) {
	ATabPageBase* page = qobject_cast<ATabPageBase*>(tabWidget->widget(index));
	emit tabWidgetCurrentChanged(index, this, tabWidget, page);
}

void MainWindow::tabBarClicked(int index) {
	ATabPageBase* page = qobject_cast<ATabPageBase*>(tabWidget->widget(index));
	emit tabWidgetTabBarClicked(index, this, tabWidget, page);
}

void MainWindow::tabBarDoubleClicked(int index) {
	ATabPageBase* page = qobject_cast<ATabPageBase*>(tabWidget->widget(index));
	emit tabWidgetTabBarDoubleClicked(index, this, tabWidget, page);
}

inline widgetUI::MsgPage* MainWindow::msgPageInstance(widgetUI::MsgPage* msgPage
	, bool set) {
	static widgetUI::MsgPage* instance = Q_NULLPTR;
	if( set )
		instance = msgPage;
	return instance;
}

inline widgetUI::LoadDataPage* MainWindow::loadDataPageInstance(widgetUI::LoadDataPage* msgPage
	, bool set) {
	static widgetUI::LoadDataPage* instance = Q_NULLPTR;
	if( set )
		instance = msgPage;
	return instance;
}

inline widgetUI::PropertyPage* MainWindow::propertyPageInstance(widgetUI::PropertyPage* msgPage
	, bool set) {
	static widgetUI::PropertyPage* instance = Q_NULLPTR;
	if( set )
		instance = msgPage;
	return instance;
}

inline widgetUI::AlgorithmPage* MainWindow::algorithmInstance(widgetUI::AlgorithmPage* msgPage
	, bool set) {
	static widgetUI::AlgorithmPage* instance = Q_NULLPTR;
	if( set )
		instance = msgPage;
	return instance;
}

inline widgetUI::CommunicationPage* MainWindow::commPageInstance(widgetUI::CommunicationPage* msgPage
	, bool set) {
	static widgetUI::CommunicationPage* instance = Q_NULLPTR;
	if( set )
		instance = msgPage;
	return instance;
}

inline widgetUI::MenuPage* widgetUI::MainWindow::menuPageInstance(widgetUI::MenuPage* msgPage
	, bool set) {
	static widgetUI::MenuPage* instance = Q_NULLPTR;
	if( set )
		instance = msgPage;
	return instance;
}

widgetUI::MsgPage* MainWindow::getMsgPage( ) {
	return msgPageInstance( );
}

widgetUI::LoadDataPage* MainWindow::getLoadDataPage( ) {
	return loadDataPageInstance( );
}

widgetUI::PropertyPage* MainWindow::getPropertyPage( ) {
	return propertyPageInstance( );
}

widgetUI::AlgorithmPage* MainWindow::getAlgorithmPage( ) {
	return algorithmInstance( );
}

widgetUI::CommunicationPage* MainWindow::getCommPage( ) {
	return commPageInstance( );
}

widgetUI::MenuPage* MainWindow::getMenuPage( ) {
	return menuPageInstance( );
}
