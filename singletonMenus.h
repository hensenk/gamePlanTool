﻿#ifndef H_MY__SINGLETONMENUS_H__H
#define	H_MY__SINGLETONMENUS_H__H

namespace staticMenu {
	class menus;
}

#include "SappInstance.h"

class appInstances::Menus {
public:
	static staticMenu::menus* GetSigInstanceInstance(bool deleteFlag = false);
};
#endif // H_MY__SINGLETONMENUS_H__H
