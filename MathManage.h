﻿#ifndef H_MY__MATHEXPRESS_H__H
#define	H_MY__MATHEXPRESS_H__H
#include "dXml.h"
#include <qhash.h>
#include <qglobal.h>
#include <QMutexLocker>
#include <QThread>

class dXml::MathExpress::MathManage {
	/*template<typename Key, typename Value>
	class mathMap : public QMap<Key, Value> {
		friend class dXml::MathExpress;
	private:
		mathMap():QMap{  }{};
	public:
		virtual ~mathMap();
	};*/


private:
	QVector<MathPairUnit*>* bodyVector;
	QHash<MathPairUnit*, bool>* manageDel;
	QString express;
	bool isRun;
	QMutex* mutex;
	MathPairUnit* currPairUnit = Q_NULLPTR;
public:
	// 初始化类
	MathManage(const QString& express);
	// 释放
	~MathManage( );
	/*
	 * 追加一个成员，并且设置该成员是否被托管
	 */
	void append(MathPairUnit* pair
		, bool manage = true);
	/*
	 * 移除成员，
	 * manage 是否释放该托管成员
	 * force 强制释放该成员（不管是否托管）
	 */
	void remove(MathPairUnit* body
		, bool manage = false
		, bool force = false);
	/*
	 * 清除成员
	 * manage 是否释放托管内存
	 * force 强制释放该成员（不管是否托管）
	 */
	void clear(bool manage = false
		, bool force = false);
	// 获取长度
	const quint32 getSize( ) const;
	/*
	 * 转换完成后是否清除
	 */
	dXml::XmlDataDoc toDxmlDoc(bool clear = false);
	/*
	 * 是否已经全部转换完毕
	 */
	const bool isRunTask( ) const;
};
#endif // H_MY__MATHEXPRESS_H__H
