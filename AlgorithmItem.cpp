﻿#include <QPushButton>
#include "AlgorithmItem.h"
#include "XmlDataUnit.h"
#include "XmlDataDoc.h"
#include <qstyle.h>
#include <qapplication.h>
#include <QCheckBox>
#include <qicon.h>
#include <QLabel>
#include <qdebug.h>
#include "AlgorithmTopItemWidget.h"

widgetUI::AlgorithmItem::~AlgorithmItem( ) {
	delete xmlDataUnit;
}

widgetUI::AlgorithmItem::AlgorithmItem(const QString& itemName
	, const QString& path
	, QWidget* parent
	, const Qt::WindowFlags& f): AAlgorithmItem(parent, f) {
	layout = new QVBoxLayout;
	setLayout(layout);
	layout->setContentsMargins(0, 0, 0, 0);
	layout->addSpacing(0);
	filePath = path;
	QLabel* label = new QLabel(filePath + "\n--------------------------------------------", this);
	layout->addWidget(label);
	layout->addSpacerItem(new QSpacerItem(20, 40, QSizePolicy::Expanding, QSizePolicy::Expanding));
	xmlDataUnit = new dXml::XmlDataUnit;
	this->itemName = itemName;
}

void widgetUI::AlgorithmItem::setExpression(const QString& express) { }

const QString widgetUI::AlgorithmItem::getItemName( ) {
	return itemName;
}

const dXml::XmlDataDoc widgetUI::AlgorithmItem::getXmlData( ) const {
	dXml::XmlDataDoc doc;
	auto&& iterator = itemMap.begin( );
	auto&& end = itemMap.end( );
	for( ;iterator != end;++iterator ) {
		// 遍历对象存储
		AlgorithmTopItemWidgetItem* const item = iterator.value( );
		QString nodeName = item->checkBox->text( );
		QString text = item->expressLabel->text( );
		dXml::XmlDataUnit* unit = new dXml::XmlDataUnit;
		unit->setName(nodeName);
		unit->setText(text);
		doc.appendChildren(unit);
	}
	return doc;
	return doc;
}

const QIcon widgetUI::AlgorithmItem::getIcon( ) {
	static QIcon icon = QApplication::style( )->standardIcon(( QStyle::StandardPixmap )30);
	return icon;
}

QVBoxLayout* widgetUI::AlgorithmItem::mainLayout( ) {
	return layout;
}

QWidget* widgetUI::AlgorithmItem::mainWidget( ) {
	return widget;
}

void widgetUI::AlgorithmItem::appItem(const QString& name
	, const QString& expree) {
	// 校验是否存在同样的公式
	if( name.isEmpty( ) || expree.isEmpty( ) || itemMap.contains(name) )
		return;
	AlgorithmTopItemWidgetItem* item = new AlgorithmTopItemWidgetItem(name, expree, this);
	connect(item, &AlgorithmTopItemWidgetItem::sigCheckChicked, this, &widgetUI::AlgorithmItem::stateChanged);
	connect(item, &AlgorithmTopItemWidgetItem::sigPushChicked, this, &widgetUI::AlgorithmItem::stateBtnChanged);
	dXml::XmlDataUnit* children = new dXml::XmlDataUnit;
	children->setName(name);
	children->setText(expree);
	layout->insertWidget(0, item);
	itemMap.insert(name, item);
}

void widgetUI::AlgorithmItem::stateChanged(AlgorithmTopItemWidgetItem* item
	, bool status) {}

void widgetUI::AlgorithmItem::stateBtnChanged(AlgorithmTopItemWidgetItem* item
	, int status) {
	layout->removeWidget(item);
	itemMap.remove(item->checkBox->text( ));
	delete item;
}
