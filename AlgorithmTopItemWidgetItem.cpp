﻿#include "AlgorithmTopItemWidgetItem.h"
#include <QLabel>
#include <qcheckbox.h>
#include <qpushbutton.h>
#include <QVBoxLayout>

AlgorithmTopItemWidgetItem::AlgorithmTopItemWidgetItem(const QString& name
	, const QString& express
	, QWidget* parent
	, const Qt::WindowFlags& f) {
	QVBoxLayout* qvBoxLayout = new QVBoxLayout;
	hLayout = new QHBoxLayout;
	qvBoxLayout->addLayout(hLayout);
	expressLabel = new QLabel(express, this);
	qvBoxLayout->addWidget(expressLabel);
	checkBox = new QCheckBox(name, this);
	hLayout->addWidget(checkBox);
	pushButton = new QPushButton("删除", this);
	hLayout->addWidget(pushButton);
	setLayout(qvBoxLayout);
	connect(this->checkBox, &QCheckBox::stateChanged, this, &AlgorithmTopItemWidgetItem::onCheckBox);
	connect(this->pushButton, &QPushButton::clicked, this, &AlgorithmTopItemWidgetItem::onBtnChicked);
}

void AlgorithmTopItemWidgetItem::onBtnChicked(bool clicked) {
	emit sigPushChicked(this, clicked);
}

void AlgorithmTopItemWidgetItem::onCheckBox(int status) {
	emit sigCheckChicked(this, status);
}
