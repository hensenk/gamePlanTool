﻿#ifndef H_MY__SIGINSTANCE_H__H
#define	H_MY__SIGINSTANCE_H__H
#include <QMutexLocker>
#include <QObject>
#include "MyTreeWidgetItemData.h"
#include "SappInstance.h"
#include "LoadDiscDirInfoThread.h"
class QTreeWidget;
class QLineEdit;
class QPushButton;
class QWidget;

class SigInstance : public QObject /*: public ISigAbstarct*/ {
Q_OBJECT;
public:
public: // 窗口主体
	widgetUI::MainWindow* mainWindow = Q_NULLPTR;

	SigInstance(QObject* parent
		, widgetUI::MainWindow* mainWindow) : QObject{ parent },
		mainWindow{ mainWindow } {}

	// 响应 QTabWidget
	// 被改变
	void tabWidgetCurrentChanged(int index
		, QWidget* sendSignalObj
		, QTabWidget* tabBody
		, widgetUI::ATabPageBase* page);
	// 单击页面
	void tabWidgetTabBarClicked(int index
		, QWidget* sendSignalObj
		, QTabWidget* tabBody
		, widgetUI::ATabPageBase* page);
	// 双击页面
	void tabWidgetTabBarDoubleClicked(int index
		, QWidget* sendSignalObj
		, QTabWidget* tabBody
		, widgetUI::ATabPageBase* page);
public: // 加载页面的响应
	// 按钮全局
	void onClick(QWidget* mainWidget
		, QPushButton* controlObj
		, bool status
		, QTreeWidget* treeWidget
		, QLineEdit* filePath);
	// 更新
	void updateBtn(QWidget* mainWidget
		, QPushButton* controlObj
		, bool status
		, QTreeWidget* treeWidget
		, QLineEdit* filePath);
	// 菜单
	void menuBtn(QWidget* mainWidget
		, QPushButton* controlObj
		, bool status
		, QTreeWidget* treeWidget
		, QLineEdit* filePath);
	void m_upDirBtn(QLineEdit* filePath);
	// 上一级
	void upDirBtn(QWidget* mainWidget
		, QPushButton* controlObj
		, bool status
		, QTreeWidget* treeWidget
		, QLineEdit* filePath);
	// 程序目录
	void resetPathDirBtn(QWidget* mainWidget
		, QPushButton* controlObj
		, bool status
		, QTreeWidget* treeWidget
		, QLineEdit* filePath);
	// 停止更新
	void stopUpdateBtn(QWidget* mainWidget
		, QPushButton* controlObj
		, bool status
		, QTreeWidget* treeWidget
		, QLineEdit* filePath);
public:
	// 文件列表触发
	void currentItemChanged(QTreeWidgetItem* current
		, QTreeWidgetItem* previous);
public:
	// 线程更新完毕
	void overLoadDataThread(lThread::LoadDiscDirInfoThread* threadObj
		, QTreeWidget* treeWidget
		, QString rootFilePath
		, QString timeFromat
		, QList<MyTreeWidgetItemData*>* treeList
		, int runCode);
	// 线程开始
	void startLoadDataThread(lThread::LoadDiscDirInfoThread* threadObj);
	// 线程任务
	void execute(int taskNumber
		, int taskIndex
		, const QString& pathFull
		, int status);
public:
	// 列表被点击
	void listClick(MyListWidgetItem* item);
	// 人物创建按钮被点击
	void creatorPerson(widgetUI::MainWindow* eventObj
		, QPushButton* btn
		, bool Clicked);
protected:
public:
	~SigInstance( );
protected:
	static QMutex* m_mutex( );
};
#endif // H_MY__SIGINSTANCE_H__H
