﻿#ifndef H_MY__TOOLBOXPAGE_H__H
#define	H_MY__TOOLBOXPAGE_H__H
#include <QWidget>

namespace dXml {
	class XmlDataUnit;
}

class QRadioButton;

namespace widgetUI {
	class AToolBoxPage;
}

class widgetUI::AToolBoxPage : public QWidget {
Q_OBJECT;
public:
	AToolBoxPage(QWidget* paren = Q_NULLPTR);
	// 当前页面被激活
	virtual void activation(QObject* obj, int index) = 0;
	// 获取名称
	virtual QString getName( ) = 0;
	// 设置名称
	virtual void setName(QString& name) = 0;
	// 获取图标
	virtual QIcon getIcon( ) = 0;
	// 设置图标
	virtual void setIcon(QIcon& icon) = 0;
	// 设置组的对话框标题
	virtual void setTitle(QString& str) = 0;
	// 复制设置 xml 对象
	virtual dXml::XmlDataUnit getXmlUnityToCloneObj( ) = 0;
	// 同步 xml 节点
	virtual bool synchXmlUnit( ) = 0;
	// 排序
	virtual void sortWidget(bool order = false) = 0;
	virtual void clear() = 0;
signals:
	void toggled(widgetUI::AToolBoxPage* page, QWidget& msg, bool isChicked);
};
#endif // H_MY__TOOLBOXPAGE_H__H
