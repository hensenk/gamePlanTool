﻿#ifndef H_MY__ALGORITHMTOPITEMWIDGET_H__H
#define	H_MY__ALGORITHMTOPITEMWIDGET_H__H
#include <qaction.h>
#include "XmlDataDoc.h"
#include "AAlgorithmItem.h"
#include <QCheckBox>
#include <QLabel>
class AlgorithmTopItemWidgetItem;

namespace widgetUI {
	class AlgorithmTopItemWidget;
}

class widgetUI::AlgorithmTopItemWidget : public widgetUI::AAlgorithmItem {
Q_OBJECT;
private:
	QHash<QString, AlgorithmTopItemWidgetItem*> itemMap;
	QVBoxLayout* itemLayout;
public:
	AlgorithmTopItemWidget(QWidget* parent = Q_NULLPTR
		, const Qt::WindowFlags& f = Qt::WindowFlags( ));
	~AlgorithmTopItemWidget();
	const QString getItemName( ) override;
	const dXml::XmlDataDoc getXmlData( ) const override;
	QVBoxLayout* mainLayout( ) override;
	QWidget* mainWidget( ) override;
	const QIcon getIcon( ) override;
	void addItem(const QString& name
		, const QString& express);
private:
	void stateChanged(AlgorithmTopItemWidgetItem* item
		, int state);
	void stateBtnChanged(AlgorithmTopItemWidgetItem* item
		, bool state);
signals:
	void pageStateChanged(AlgorithmTopItemWidgetItem* item
		, int state);
};
#endif // H_MY__ALGORITHMTOPITEMWIDGET_H__H
