﻿#include "mainwindow.h"
#include <QApplication>
#include "MyApplication.h"

void testXML( ) {
	dXml::XmlDataDoc leftDoc;
	dXml::XmlDataUnit* unit = new dXml::XmlDataUnit("22");
	leftDoc.appendChildren(unit);
	unit = new dXml::XmlDataUnit("22");
	leftDoc.appendChildren(unit);
	unit = new dXml::XmlDataUnit("24");
	leftDoc.appendChildren(unit);
	unit = new dXml::XmlDataUnit("25");
	leftDoc.appendChildren(unit);
	unit = new dXml::XmlDataUnit("26");
	leftDoc.appendChildren(unit);
	unit = new dXml::XmlDataUnit("27");
	leftDoc.appendChildren(unit);
	dXml::XmlDataDoc rightDoc = leftDoc;
	dXml::XmlDataDoc overDoc = dXml::xmlBlend(leftDoc, rightDoc, 1);
}

int main(int argc
	, char* argv[]) {
	/*testXML( );
	return 0;*/
	MyApplication myAppRun(argc, argv);
	widgetUI::MainWindow* mainWindow = new widgetUI::MainWindow;
	myAppRun.setWinProcessing(mainWindow);
	mainWindow->show( );
	int execResult;
	execResult = myAppRun.exec( );
	delete mainWindow;
	return execResult;
}
